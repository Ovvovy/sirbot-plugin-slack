"""
Hookimpl for the slack plugins
"""

import pluggy

hookimpl = pluggy.HookimplMarker('sirbot.slack')
