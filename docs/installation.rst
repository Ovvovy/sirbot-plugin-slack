.. _installation:

============
Installation
============


Stable release
--------------

**NOT YET RELEASED**

To install sirbot-plugin-slack, run this command in your terminal:

.. code-block:: console

    $ pip install sirbot-plugin-slack

This is the preferred method to install sirbot-plugin-slack, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for sirbot-plugin-slack can be downloaded from the `gitlab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://gitlab.com/PythonDevCommunity/sirbot-plugin-slack

Or download the `tarball`_:

.. code-block:: console

    $ curl  -OL https://gitlab.com/PythonDevCommunity/sir-bot-a-lot/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ pip install sirbot-plugin-slack/


.. _gitlab repo: https://gitlab.com/PythonDevCommunity/sirbot-plugin-slack
.. _tarball: https://gitlab.com/PythonDevCommunity/sirbot-plugin-slack/tarball/master
